package com.example.demo;

import com.example.demo.web.BankCard;
import com.example.demo.web.KucherukService;
import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class DemoApplication {

    private final KucherukService service;

    @GetMapping("/")
    String home() {
        return "Spring is here!";
    }

    @PostMapping("/{middleName}")
    public String sayHi(@RequestParam String name, @RequestBody String lastName, @PathVariable String middleName) {
        return "Hello " + name + " " + lastName + " " + middleName + "!";
    }

    @GetMapping("/card/{id}")
    public String getCard(@PathVariable Long id) {
        return service.getCard(id).toString();
    }

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    public DemoApplication(KucherukService service) {
        this.service = service;
    }
}