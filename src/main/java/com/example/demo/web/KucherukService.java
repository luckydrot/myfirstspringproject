package com.example.demo.web;

import org.apache.tomcat.util.codec.binary.BaseNCodec;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
public class KucherukService {

    private final BankCardRepository repository;

    public BankCard getCard(Long cardId) {
        return repository.getCardById(cardId)
                .orElseThrow(RuntimeException::new);
    }

    public KucherukService(BankCardRepository repository) {
        this.repository = repository;
    }
}
