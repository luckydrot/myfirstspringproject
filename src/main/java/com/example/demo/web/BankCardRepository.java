package com.example.demo.web;

import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class BankCardRepository {

    List<BankCard> bankCards;

    public Optional<BankCard> getCardById(Long cardId) {
        return bankCards.stream()
                .filter(card -> cardId.equals(card.getId()))
                .findFirst();
    }

    @PostConstruct
    private void init(){
        bankCards = new ArrayList<>();
        bankCards.add(new BankCard(1L, "123"));
    }
}
