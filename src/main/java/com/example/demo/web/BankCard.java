package com.example.demo.web;

public class BankCard {

    private Long id;

    private String cardNumber;

    public BankCard(Long id, String cardNumber) {
        this.id = id;
        this.cardNumber = cardNumber;
    }

    public Long getId() {
        return id;
    }

    @Override
    public String toString() {
        return "BankCard (id = " + id
                + " number = " + cardNumber
                + ")";
    }
}
